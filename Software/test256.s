; Dragon 256K Banker Board test program
; S.Orchard 2021
;
; Adapted from my Dragon 32/64 memory test software
;
;
; Assemble with asm6809 (www.6809.org.uk/asm6809)
;
; create a DragonDos binary:
;    asm6809 -D -o TEST256.BIN test256.s
;
; create a rom image:
;    asm6809 -o test256.rom test256.s
;


 if EMU
; fake bank registers
BANKREG equ $ffe0
 else
; real bank registers
BANKREG equ $ff30
 endif


; number of digits in pass counter
COUNTER_DIGITS  equ 6


; branch and link macro
; performs relative subroutine call with return address in s
; subroutine should return with jmp ,s
brl     macro
        leas \1,pcr
        exg s,pc
        endm

; macro that displays text message
show_str macro
        leau \1,pcr
        brl vdu_outstrz
        endm


        org $2000

start_address

        orcc #$50
        leay seq_start,pcr

        brl vdu_cls

        ldx PRM_VDU_ADDR,y
        leau 32,x
        ldd #$3000 + COUNTER_DIGITS
1       sta ,-u
        decb
        bne 1b

        show_str txt_title
        
        show_str txt_ram256

mloop
        brl vdu_set_mode
        lda PRM_FUNCTION,y      ; function id
        leax fn_table,pcr       ; function table
        ldd a,x                 ; look up function offset
        leax fn_base_addr,pcr   ; get function base address ...
        jmp d,x                 ; ... and jump to offset

; -------------------------------------
; fn_base_addr defines reference point to calculate function address
; actual value doesn't matter other than ideally a small offset from pc
; in function jump calculation

fn_base_addr

; -------------------------------------
; Check if running from ROM
; Attempts to change a memory location within the program image.
; If value changes, then assume running from RAM.
; Otherwise adjust sequence pointer to new point in sequence.

fn_romcheck
        leax mem_test_var,pcr
        lda ,x
        com ,x
        cmpa ,x
        beq 5f  ; value didn't change - probably rom

        sta $ffdf       ; switch to map1
        show_str txt_ram_resident
        leay 5,y
1       bra mloop

5       show_str txt_rom_resident
        ldd PRM_OFFSET,y
        leay d,y
        bra 1b

; -------------------------------------
; Set lower 32K bank
fn_set_bank_low32
        show_str txt_set_bank_low32
        lda 3,y
        ldu #BANKREG
        sta a,u
        sta 9,u         ; fix video in bank 0
        brl vdu_outhexdig
        leay 4,y
        lbra mloop

; Set upper 32K bank
fn_set_bank_high32
        show_str txt_set_bank_high32
        lda 3,y
        ldu #BANKREG+4
        sta a,u
        sta 8,u         ; set video bank = lower 32K bank
        brl vdu_outhexdig
        leay 4,y
        lbra mloop

; -------------------------------------
; macro that displays test address range

show_test_range macro
        show_str txt_address
        ldd PRM_TEST_START,y
        brl vdu_outhex16
        lda #'-
        sta ,x+
        ldd PRM_TEST_END,y
        brl vdu_outhex16
        endm

; -------------------------------------
; perform walking bit memory tests

fn_memtest_walk
        show_test_range
        show_str txt_walking1
        brl walking1
        show_str txt_walking0
        brl walking0
        leay 7,y
        lbra mloop

; -------------------------------------
; write pattern to memory

fn_pattern_write
        show_test_range
        show_str txt_pattern_write
        brl ptn_write
        leay 9,y
        lbra mloop

fn_anti_ptn_write
        show_test_range
        show_str txt_anti_ptn_write
        brl anti_ptn_write
        leay 9,y
        lbra mloop

; -------------------------------------
; approx n x 0.5s delay @ 895KHz

fn_delay
        show_str txt_delay
        ldb PRM_DELAY,y
        ldx #55937
1       leax -1,x
        bne 1b
        decb
        bne 1b
        leay 4,y
        lbra mloop

; -------------------------------------
; Check pattern in memory

fn_pattern_check
        show_test_range
        show_str txt_pattern_check
        brl ptn_check
        leay 9,y
        lbra mloop

fn_anti_ptn_check
        show_test_range
        show_str txt_anti_ptn_check
        brl anti_ptn_check
        leay 9,y
        lbra mloop

; -------------------------------------
; copy program and display to new addresses
; jumps into program at new address
;
fn_relocate
        leas start_address,pcr          ; copy program
        ldu PRM_NEW_ADDR,y              ;
        ldx #end_address-start_address  ;
1       lda ,s+                         ;
        sta ,u+                         ;
        leax -1,x                       ;
        bne 1b                          ;

        tfr s,d         ; compute program relocation offset
        comb            ; then adjust y to point
        coma            ; to relocated params
        addd #1         ;
        leau d,u        ;
        tfr u,d         ;
        leay d,y        ;

        lds PRM_VDU_ADDR,y      ; copy video display
        ldu PRM_NEW_VDU,y       ;
        clrb                    ;
1       ldx ,s++                ;
        stx ,u++                ;
        decb                    ;
        bne 1b                  ;

        ldx PRM_NEW_ADDR,y
        leay 7,y
        jmp mloop-start_address,x

; -------------------------------------
; copy display to new address
;
fn_relocate_vid
        lds PRM_VDU_ADDR,y
        ldu PRM_NEW_VDU,y
        clrb
1       ldx ,s++
        stx ,u++
        decb
        bne 1b

        leay 5,y
        lbra mloop

; -------------------------------------
; adjust parameter pointer by specified offset
;
fn_seqjump
        ldd PRM_OFFSET,y
        leay d,y
        lbra mloop

; -------------------------------------
; update on-screen counter
; directly manipulates characters in video ram
;
fn_count
        ldx PRM_VDU_ADDR,y
        leax 32,x
        ldb #COUNTER_DIGITS
4       coma    ; set carry
5       lda ,-x
        adca #0
        cmpa #'9
        bls 1f
        lda #'0
        sta ,x
        decb
        bne 4b
1       sta ,x
        clra    ; clear carry
        decb
        bne 5b

        leay 3,y
        lbra mloop

; -------------------------------------

; walking bit 0
walking0
        ldx PRM_TEST_START,y
1       ldd #$7f7f
2       std ,x
        cmpd ,x
        bne 9f          ; error
        lsra
        rorb
        ora #128
        bcs 2b
        leax 2,x
        cmpx PRM_TEST_END,y
        bls 1b
        jmp ,s


; walking bit 1
walking1
        ldx PRM_TEST_START,y
1       ldd #$8080
2       std ,x
        cmpd ,x
        bne 9f          ; error
        lsra
        lsrb
        bne 2b
        leax 2,x
        cmpx PRM_TEST_END,y
        bls 1b
        jmp ,s

9       cmpa ,x         ; check which address failed
        bne fail        ; failed at x
        tfr b,a         ; failed at x+1
        leax 1,x
        bra fail


PATTERN_INC     equ 113         ; relatively prime to 256

ptn_write
        ldx PRM_TEST_START,y
        ldd PRM_SEED,y
1       sta ,x+
2       addd #PATTERN_INC*257
        beq 2b          ; reduce cycle length to 65535
        cmpx PRM_TEST_END,y
        bls 1b
        jmp ,s

anti_ptn_write
        ldx PRM_TEST_START,y
        ldd PRM_SEED,y
1       coma
        sta ,x+
        coma
2       addd #PATTERN_INC*257
        beq 2b          ; reduce cycle length to 65535
        cmpx PRM_TEST_END,y
        bls 1b
        jmp ,s

ptn_check
        ldx PRM_TEST_START,y
        ldd PRM_SEED,y
1       cmpa ,x+
        bne 9f          ; error
2       addd #PATTERN_INC*257
        beq 2b          ; reduce cycle length to 65535
        cmpx PRM_TEST_END,y
        bls 1b
        jmp ,s

anti_ptn_check
        ldx PRM_TEST_START,y
        ldd PRM_SEED,y
1       coma
        cmpa ,x+
        bne 9f          ; error
        coma
2       addd #PATTERN_INC*257
        beq 2b          ; reduce cycle length to 65535
        cmpx PRM_TEST_END,y
        bls 1b
        jmp ,s

9       leax -1,x       ; correct to failed address

; display failure info
; x = address
; a = expected byte

fail
        leau ,x         ; save failed address

        ldx PRM_VDU_ADDR,y
        leax 205,x
        brl vdu_outhex8 ; display expected byte
        leax 30,x
        lda ,u          ;
        brl vdu_outhex8 ; display byte from failed address

        leax 30-96,x
        tfr u,d
        brl vdu_outhex16        ; display failed address

        lda $ff22
        eora #8
        sta $ff22

        show_str txt_fail
        bra *                   ; halt program


; -------------------------------------

; output 16 bit hex value
; d = value
; x = screen address

vdu_outhex16
        leau ,s
        brl vdu_outhex8
        tfr b,a
        leas ,u

; output 8 bit hex value
; a = value
; x = screen address

vdu_outhex8
        tfr a,dp
        lsra
        lsra
        lsra
        lsra
        adda #'0
        cmpa #'9
        bls 1f
        adda #1-10-'0
1       sta ,x+
        tfr dp,a
vdu_outhexdig
        anda #15
        adda #'0
        cmpa #'9
        bls 1f
        adda #1-10-'0
1       sta ,x+
        jmp ,s

; display a string
; u points to screen offset followed by
; null terminated string

vdu_outstrz
        ldx PRM_VDU_ADDR,y
        ldd ,u++        ; add offset to video start
        leax d,x        ;
5       lda ,u+         ; get character
        bne 1f          ; display if non-zero
        jmp ,s          ; else return

1       cmpa #$0d       ; check for CR
        bne 1f          ;
        exg d,x         ; move to start of next line
        andb #$e0       ;
        addd #$20       ;
        exg d,x         ;
        bra 5b          ;

1       cmpa #$40       ; map char code
        blo 2f          ; into 6847 code
        cmpa #$60       ;
        blo 1f          ;
        eora #$60       ;
1       eora #$40       ;
2       sta ,x+         ;
        bra 5b          ;


; clear screen

vdu_cls
        ldx PRM_VDU_ADDR,y
        ldu #$2020
        clrb
1       stu ,x++
        decb
        bne 1b
        jmp ,s


; set display to text mode and setup base address

vdu_set_mode
        clra            ; set text mode
        sta $ff22       ;
        ldx #$ffc6
        sta -6,x        ; set 512 byte mem mode
        sta -4,x        ;
        sta -2,x        ;
        lda PRM_VDU_ADDR,y
        lsra            ; set video base address
1       clrb            ;
        lsra            ;
        rolb            ;
        sta b,x         ;
        leax 2,x        ;
        cmpx #$ffd4     ;
        blo 1b          ;
        jmp ,s

; -------------------------------------
; strings for display
; first param is position on screen
; followed by null terminated string

txt_title
        fdb 0
        fcb /MEMORY TEST/,0

txt_address
        fdb 64
        fcb /TESTING /,0

txt_pattern_write
        fdb 96
        fcb /PATTERN WRITE      /,0

txt_anti_ptn_write
        fdb 96
        fcb /ANTI-PATTERN WRITE /,0

txt_delay
        fdb 96
        fcb /DELAY              /,0

txt_pattern_check
        fdb 96
        fcb /PATTERN CHECK      /,0

txt_anti_ptn_check
        fdb 96
        fcb /ANTI-PATTERN CHECK /,0

txt_walking0
        fdb 96
        fcb /WALKING ANTI-BIT  /,0

txt_walking1
        fdb 96
        fcb /WALKING BIT       /,0

txt_ram_resident
        fdb 32
        fcb /RAM RESIDENT/,0

txt_rom_resident
        fdb 32
        fcb /ROM RESIDENT/,0

txt_ram256
        fdb 16
        fcb /256K/,0

txt_set_bank_low32
        fdb 82
        fcb /BNKL /,0

txt_set_bank_high32
        fdb 89
        fcb /BNKH /,0

txt_fail
        fdb 160
        fcb /*** FAILED @ /,13
        fcb /*** EXPECTED /,13
        fcb /*** FOUND    /,0

; -------------------------------------
; memory location used for rom-resident check

mem_test_var    fdb $55aa

; -------------------------------------
; table of functions

; macro that creates a jump table entry
; first an EQUate to form an offset into the jump table
; then an address offset to point to the function

fn_entry macro
\1      equ (* - fn_table)
        fdb \2 - fn_base_addr
        endm

fn_table
        fn_entry "ROMCHECK",            fn_romcheck
        fn_entry "BANKLOW32",           fn_set_bank_low32
        fn_entry "BANKHIGH32",          fn_set_bank_high32
        fn_entry "MEMTEST_WALK",        fn_memtest_walk
        fn_entry "PATTERN_WRITE",       fn_pattern_write
        fn_entry "ANTI_PATTERN_WRITE",  fn_anti_ptn_write
        fn_entry "DELAY",               fn_delay
        fn_entry "PATTERN_CHECK",       fn_pattern_check
        fn_entry "ANTI_PATTERN_CHECK",  fn_anti_ptn_check
        fn_entry "RELOCATE_ALL",        fn_relocate
        fn_entry "RELOCATE_VID",        fn_relocate_vid
        fn_entry "SEQJUMP",             fn_seqjump
        fn_entry "COUNT",               fn_count


; memtest parameters
PRM_FUNCTION    equ 0
PRM_VDU_ADDR    equ 1
PRM_TEST_START  equ 3
PRM_TEST_END    equ 5
PRM_SEED        equ 7

; delay param
PRM_DELAY       equ 3

; relocate params
PRM_NEW_VDU     equ 3
PRM_NEW_ADDR    equ 5

; sequence jump param
PRM_OFFSET      equ 3

; -------------------------------------
; Supporting macros for test sequence

; check if ram or rom resident
rom_check macro
1       fcb ROMCHECK
        fdb \1          ; video address
        fdb \2-1b       ; jump offset for rom
        endm

; set lower 32K bank
set_bank_low32 macro
        fcb BANKLOW32
        fdb \1          ; video address
        fcb \2          ; bank number
        endm

; set upper 32K bank
set_bank_high32 macro
        fcb BANKHIGH32
        fdb \1          ; video address
        fcb \2          ; bank number
        endm

; relocate program and video display to new addresses
relocate_all macro
        fcb RELOCATE_ALL
        fdb \1          ; video address
        fdb \2          ; new video address
        fdb \3          ; new program address
        endm

; relocate video display to new address
relocate_vid macro
        fcb RELOCATE_VID
        fdb \1          ; video address
        fdb \2          ; new video address
        endm

; walking bit memory tests
walking_tests macro
        fcb MEMTEST_WALK
        fdb \1          ; video address
        fdb \2          ; test start address
        fdb \3          ; test end address
        endm

; memory pattern write
pattern_write macro
        fcb PATTERN_WRITE
        fdb \1          ; video address
        fdb \2          ; test start address
        fdb \3          ; test end address
        fdb \4          ; seed
        endm

; memory anti-pattern write
anti_ptn_write macro
        fcb ANTI_PATTERN_WRITE
        fdb \1          ; video address
        fdb \2          ; test start address
        fdb \3          ; test end address
        fdb \4          ; seed
        endm

; memory pattern check
pattern_check macro
        fcb PATTERN_CHECK
        fdb \1          ; video address
        fdb \2          ; test start address
        fdb \3          ; test end address
        fdb \4          ; seed
        endm

; memory anti-pattern check
anti_ptn_check macro
        fcb ANTI_PATTERN_CHECK
        fdb \1          ; video address
        fdb \2          ; test start address
        fdb \3          ; test end address
        fdb \4          ; seed
        endm

; approx n x 0.5s delay @ 895KHz
delay_0_5s macro
        fcb DELAY
        fdb \1          ; video address
        fcb \2          ; delay
        endm

; pattern write/delay/read memory tests
pattern_tests macro
        pattern_write   \1,\2,\3,$0000
        delay_0_5s      \1,2
        pattern_check   \1,\2,\3,$0000
        anti_ptn_write  \1,\2,\3,$0000
        delay_0_5s      \1,2
        anti_ptn_check  \1,\2,\3,$0000
        endm

mem_tests macro
        walking_tests \1,\2,\3
        pattern_tests \1,\2,\3
        endm

; update on-screen pass count
update_count macro
        fcb COUNT
        fdb \1          ; video address
        endm

; jump to new sequence point
seq_jump macro
1       fcb SEQJUMP
        fdb \1          ; video address
        fdb \2-1b       ; jump offset
        endm

; -------------------------------------
; Test sequence entry point
; The sequence is formed from a list of functions and their parameters
; The first parameter is always the video display address
; For test functions, the 2nd and 3rd parameters are start and end addresses
; The pattern check 4th parameter is the pattern seed


seq_start

        rom_check       $0000,seq_rom_256

; -------------------------------------
; RAM resident test sequence
;

bank_pattern_fn macro
        fcb \2  ; BANKLOW32 / BANKHIGH32
        fdb \1  ; video address
        fcb \3  ; bank number
        fcb \4  ; PATTERN_WRITE / ANTI_PATTERN_WRITE / PATTERN_CHECK / ANTI_PATTERN_CHECK
        fdb \1  ; video address
        fdb \5  ; test start address
        fdb \6  ; test end address
  if !EMU
        fdb \7  ; seed
  else
        fdb 0
  endif
        endm

bank_pattern_series macro
        bank_pattern_fn \1,\2,$03,\3,\4,\5,$0000
        bank_pattern_fn \1,\2,$02,\3,\4,\5,$1111
        bank_pattern_fn \1,\2,$01,\3,\4,\5,$2222
        bank_pattern_fn \1,\2,$00,\3,\4,\5,$3333
        endm

mem_tests_high_bank macro
        set_bank_high32 $0000,\1
        mem_tests       $0000,$4000,$feff
        endm

mem_tests_low_bank macro
        set_bank_low32  $c000,\1
        mem_tests       $c000,$0000,$bfff
        endm


seq_ram_256

        ; run fast pattern checks first to confirm banks are working

        ; pattern tests in upper 32K banks
        
        bank_pattern_series     $0000,BANKHIGH32,PATTERN_WRITE,$8000,$feff
        delay_0_5s              $0000,2
        bank_pattern_series     $0000,BANKHIGH32,PATTERN_CHECK,$8000,$feff

        bank_pattern_series     $0000,BANKHIGH32,ANTI_PATTERN_WRITE,$8000,$feff
        delay_0_5s              $0000,2
        bank_pattern_series     $0000,BANKHIGH32,ANTI_PATTERN_CHECK,$8000,$feff


         ; pattern tests in lower 32K banks

        relocate_all            $0000,$8000,$8200

        bank_pattern_series     $8000,BANKLOW32,PATTERN_WRITE,$0000,$7fff
        delay_0_5s              $8000,2
        bank_pattern_series     $8000,BANKLOW32,PATTERN_CHECK,$0000,$7fff

        bank_pattern_series     $8000,BANKLOW32,ANTI_PATTERN_WRITE,$0000,$7fff
        delay_0_5s              $8000,2
        bank_pattern_series     $8000,BANKLOW32,ANTI_PATTERN_CHECK,$0000,$7fff


        ; run longer tests across 32K boundary
        
        relocate_all    $8000,$0000,$0200

        mem_tests_high_bank 3
        mem_tests_high_bank 2
        mem_tests_high_bank 1
        mem_tests_high_bank 0

        relocate_all    $0000,$c000,$c200

        mem_tests_low_bank 3
        mem_tests_low_bank 2
        mem_tests_low_bank 1
        mem_tests_low_bank 0

        relocate_all    $c000,$0000,$0200
        
        update_count    $0000
        seq_jump        $0000,seq_ram_256

; -------------------------------------
; ROM resident test sequence
; Performs initial RAM check then copies itself
; to RAM and continues with RAM resident sequence

seq_rom_256
        mem_tests       $0000,$0200,$7fff
        relocate_all    $0000,$0000,$0200
        seq_jump        $0000,seq_start


end_address

        ; pad image for rom
        align 256,0


